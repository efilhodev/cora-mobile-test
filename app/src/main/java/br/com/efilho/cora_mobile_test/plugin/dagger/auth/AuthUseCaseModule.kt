package br.com.efilho.cora_mobile_test.plugin.dagger.auth

import android.content.Context
import br.com.efilho.cora_mobile_test.feature.auth.business.interactor.AuthRepository
import br.com.efilho.cora_mobile_test.feature.auth.business.interactor.AuthUseCase
import br.com.efilho.cora_mobile_test.plugin.dao.AuthDAO
import br.com.efilho.cora_mobile_test.plugin.dao.SharedPreferencesDAO
import br.com.efilho.cora_mobile_test.plugin.repository.AuthRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Named

@Module
class AuthUseCaseModule(private val context: Context) {

    @Provides
    @Reusable
    fun providerAuthUseCase(repository: AuthRepository): AuthUseCase {
        return AuthUseCase(repository)
    }

    @Provides
    @Reusable
    fun providerAuthRepository( url: String, dao: AuthDAO): AuthRepository {
        return AuthRepositoryImpl(url, dao)
    }

    @Provides
    @Reusable
    fun provideAuthDAO(): AuthDAO {
        return SharedPreferencesDAO(context)
    }

    @Provides
    @Reusable
    fun providerRepositoryUrl(): String {
        return "https://connect-sandbox.moip.com.br/"
    }
}