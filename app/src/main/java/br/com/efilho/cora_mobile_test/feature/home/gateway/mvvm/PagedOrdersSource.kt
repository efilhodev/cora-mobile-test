package br.com.efilho.cora_mobile_test.feature.home.gateway.mvvm

import androidx.paging.PageKeyedDataSource
import br.com.efilho.cora_mobile_test.base.business.data.Order
import br.com.efilho.cora_mobile_test.feature.home.business.data.PagedOrdersRequest
import br.com.efilho.cora_mobile_test.feature.home.business.interactor.GetOrdersUseCase
import androidx.lifecycle.MutableLiveData
import br.com.efilho.cora_mobile_test.base.business.interactor.ErrorOutput
import br.com.efilho.cora_mobile_test.base.business.interactor.Output
import br.com.efilho.cora_mobile_test.base.business.interactor.ValueOutput
import kotlinx.coroutines.Dispatchers

class PagedOrdersSource(private val useCase: GetOrdersUseCase) : PageKeyedDataSource<Int, Order>() {

    val error: MutableLiveData<Output<List<Order>>> = MutableLiveData()
    private val pagedOrdersRequest = PagedOrdersRequest(1, 30)

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Order>) {

        useCase.invoke(pagedOrdersRequest, Dispatchers.Unconfined) { response ->
            if (response.isError()) {
                error.postValue(ErrorOutput(response.error))
            } else {
                callback.onResult(response.value!!.toMutableList(), null, 2)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Order>) {
        pagedOrdersRequest.offset = params.key

        useCase.invoke(pagedOrdersRequest, Dispatchers.Unconfined) { response ->
            if (response.isError()) {
                error.postValue(ErrorOutput(response.error))
            } else {
                callback.onResult(response.value!!.toMutableList(), params.key.plus(1))
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Order>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}