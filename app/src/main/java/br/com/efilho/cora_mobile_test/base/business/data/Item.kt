package br.com.efilho.cora_mobile_test.base.business.data

data class Item(
    val category: String,
    val detail: String,
    val price: Int,
    val product: String,
    val quantity: Int
)