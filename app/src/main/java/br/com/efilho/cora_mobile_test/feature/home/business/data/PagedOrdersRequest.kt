package br.com.efilho.cora_mobile_test.feature.home.business.data

data class PagedOrdersRequest(var offset: Int = 0, val limit: Int, val filters: String = "")