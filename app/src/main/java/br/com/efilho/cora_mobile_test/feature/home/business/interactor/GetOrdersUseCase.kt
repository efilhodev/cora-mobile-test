package br.com.efilho.cora_mobile_test.feature.home.business.interactor

import br.com.efilho.cora_mobile_test.base.business.interactor.Output
import br.com.efilho.cora_mobile_test.base.business.interactor.UseCase
import br.com.efilho.cora_mobile_test.base.business.interactor.ValueOutput
import br.com.efilho.cora_mobile_test.base.business.data.Order
import br.com.efilho.cora_mobile_test.feature.home.business.data.PagedOrdersRequest

class GetOrdersUseCase(private val repository: OrderRepository) : UseCase<PagedOrdersRequest, List<Order>>() {

    override fun guard(param: PagedOrdersRequest?): Boolean {
        return param != null
    }

    override fun execute(param: PagedOrdersRequest?): Output<List<Order>> {
        val orders = repository.getOrders(param!!)
        return ValueOutput(orders)
    }
}