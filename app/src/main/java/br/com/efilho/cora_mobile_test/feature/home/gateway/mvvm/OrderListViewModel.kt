package br.com.efilho.cora_mobile_test.feature.home.gateway.mvvm

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import br.com.efilho.cora_mobile_test.base.gateway.mvvm.BaseViewModel
import br.com.efilho.cora_mobile_test.feature.home.business.interactor.GetOrdersUseCase
import br.com.efilho.cora_mobile_test.feature.home.gateway.di.OrderListViewModelInjector
import javax.inject.Inject
import androidx.paging.LivePagedListBuilder
import br.com.efilho.cora_mobile_test.base.business.data.Order
import br.com.efilho.cora_mobile_test.base.business.interactor.ValueOutput
import java.util.concurrent.Executors
import androidx.lifecycle.Transformations
import br.com.efilho.cora_mobile_test.base.business.interactor.Output
import java.util.concurrent.ExecutorService

class OrderListViewModel : BaseViewModel() {
    @Inject
    lateinit var getOrdersUseCase: GetOrdersUseCase

    private lateinit var getOrdersResponseChannel: LiveData<PagedList<Order>>
    private lateinit var getOrdersErrorChannel: LiveData<Output<List<Order>>>

    init {
        OrderListViewModelInjector.injector.inject(this)
    }

    override fun declareChannels() {
        availableChannels.add(ORDER_LIST_SUCCESS_CHANNEL)
    }

    fun getOrders() {

        val pagedOrdersDataFactory = PagedOrdersDataFactory(getOrdersUseCase)

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(LOAD_SIZE_HINT)
            .setPageSize(PAGE_SIZE).build()

        val executor = Executors.newFixedThreadPool(5)

        observeGetOrdersResponse(pagedOrdersDataFactory, pagedListConfig, executor)

        observeGetOrdersError(pagedOrdersDataFactory)

    }

    private fun observeGetOrdersError(pagedOrdersDataFactory: PagedOrdersDataFactory) {
        getOrdersErrorChannel = Transformations.switchMap(
            pagedOrdersDataFactory.mutableLiveData
        ) { it.error }

        getOrdersErrorChannel.observeForever {
            postValue(ORDER_LIST_SUCCESS_CHANNEL, it)
        }
    }

    private fun observeGetOrdersResponse(
        pagedOrdersDataFactory: PagedOrdersDataFactory,
        pagedListConfig: PagedList.Config,
        executor: ExecutorService
    ) {
        getOrdersResponseChannel = LivePagedListBuilder(pagedOrdersDataFactory, pagedListConfig)
            .setFetchExecutor(executor)
            .build()

        getOrdersResponseChannel.observeForever {
            val output = ValueOutput(it)
            postValue(ORDER_LIST_SUCCESS_CHANNEL, output)
        }
    }

    companion object {
        private const val ORDER_LIST_SUCCESS_CHANNEL = "ORDER_LIST_SUCCESS_CHANNEL"
        private const val PAGE_SIZE = 30
        private const val LOAD_SIZE_HINT = 10
    }
}