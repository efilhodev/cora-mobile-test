package br.com.efilho.cora_mobile_test.base.business.interactor

import kotlinx.coroutines.*

open class UseCaseDispatcher<in P, R>(
    private val useCase: AbstractUseCase<P, R>,
    private val resultOn: CoroutineDispatcher = Dispatchers.Main
) : Interactor<P, R> by useCase {

    fun dispatch(param: P? = null, dispatcher: CoroutineDispatcher = Dispatchers.IO, callback: (R) -> Unit): Job? {
        useCase.callback = callback
        onBeforeDispatch()
        return GlobalScope.launch(dispatcher) {
            onDispatch(param)
        }
    }

    private suspend fun onDispatch(param: P? = null) {
        try {
            onBeforeExecute()
            onExecute(param)
        } catch (error: Throwable) {
            onError(error)
        }
    }

    private suspend fun onExecute(param: P? = null) {
        if (useCase.guard(param)) {
            onSuccess(useCase.execute(param))
        } else {
            useCase.onGuardError()
        }
    }

    private suspend fun onSuccess(output: R) {
        withContext(resultOn) {
            try {
                onBeforeSuccess()
                useCase.onSuccess(output)
                onFinish()
            } catch (error: Throwable) {
                onError(error)
            }
        }
    }

    private suspend fun onError(error: Throwable) {
        withContext(resultOn) {
            try {
                onBeforeError()
                useCase.onError(error)
                onFinish()
            } catch (e: Throwable) {
                onErrorHalt(e)
            }
        }
    }

    internal open fun onBeforeDispatch() {}

    internal open fun onBeforeExecute() {}

    internal open fun onBeforeSuccess() {}

    internal open fun onBeforeError() {}

    internal open fun onFinish() {}

    internal open fun onErrorHalt(error: Throwable) {}
}