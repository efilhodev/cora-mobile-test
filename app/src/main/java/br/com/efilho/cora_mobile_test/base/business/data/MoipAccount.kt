package br.com.efilho.cora_mobile_test.base.business.data

data class MoipAccount(
    val fullname: String,
    val id: String,
    val login: String
)