package br.com.efilho.cora_mobile_test.plugin.dagger.home

import br.com.efilho.cora_mobile_test.feature.home.gateway.mvvm.OrderListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [GetOrdersUseCaseModule::class])
interface OrderListViewModelComponent {
    fun inject(orderListViewModel: OrderListViewModel)
}