package br.com.efilho.cora_mobile_test.feature.home.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import br.com.efilho.cora_mobile_test.R
import br.com.efilho.cora_mobile_test.base.business.data.Order
import br.com.efilho.cora_mobile_test.feature.home.gateway.mvvm.OrderDiff

class OrderListAdapter :
    PagedListAdapter<Order, RecyclerView.ViewHolder>(OrderDiff.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> OrderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list_order, parent, false))
            else -> throw Exception()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val order = getItem(position)
        (holder as OrderViewHolder).apply {
            this.bind(order!!)
        }
    }

    class OrderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvName: TextView = itemView.findViewById(R.id.tv_order_name)

        fun bind(order: Order) {
            tvName.text = order.id
        }
    }

    override fun getItemViewType(position: Int): Int = TYPE_ITEM

    companion object {
        const val TYPE_ITEM = 1
    }

}