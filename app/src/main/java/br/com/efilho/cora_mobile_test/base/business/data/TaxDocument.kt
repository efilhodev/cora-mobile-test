package br.com.efilho.cora_mobile_test.base.business.data

data class TaxDocument(
    val number: String,
    val type: String
)