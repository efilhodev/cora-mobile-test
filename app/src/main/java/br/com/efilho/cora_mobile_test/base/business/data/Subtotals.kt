package br.com.efilho.cora_mobile_test.base.business.data

data class Subtotals(
    val addition: Int,
    val discount: Int,
    val items: Int,
    val shipping: Int
)