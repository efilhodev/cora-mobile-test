package br.com.efilho.cora_mobile_test.base.business.data

import com.google.gson.annotations.SerializedName



data class Order (
    @SerializedName("_links") val links: Links,
    val addresses: List<Address>,
    val amount: Amount,
    val checkoutPreferences: Any,
    val createdAt: String,
    val customer: Customer,
    val events: List<Event>,
    val id: String,
    val items: List<Item>,
    val ownId: String,
    val payments: List<Payment>,
    val platform: String,
    val receivers: List<Receiver>,
    val status: String,
    val updatedAt: String){


}