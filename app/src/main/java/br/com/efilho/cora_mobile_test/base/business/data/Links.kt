package br.com.efilho.cora_mobile_test.base.business.data

data class Links(
    val hostedAccount: HostedAccount,
    val self: Self,
    val checkout: Checkout
)