package br.com.efilho.cora_mobile_test.feature.auth.business.interactor

import br.com.efilho.cora_mobile_test.base.business.interactor.Output
import br.com.efilho.cora_mobile_test.base.business.interactor.UseCase
import br.com.efilho.cora_mobile_test.base.business.interactor.ValueOutput
import br.com.efilho.cora_mobile_test.feature.auth.business.data.Credential
import br.com.efilho.cora_mobile_test.feature.auth.business.data.Token

class AuthUseCase (private val repository: AuthRepository): UseCase<Credential, Token>() {

    override fun guard(param: Credential?): Boolean {
        return param !=  null
    }

    override fun execute(param: Credential?): Output<Token> {
            val token = repository.authenticate(param!!)
            return ValueOutput(token)
    }
}