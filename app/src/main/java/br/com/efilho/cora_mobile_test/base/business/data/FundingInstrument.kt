package br.com.efilho.cora_mobile_test.base.business.data

data class FundingInstrument(
    val brand: String,
    val method: String
)