package br.com.efilho.cora_mobile_test.feature.home.gateway.mvvm

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import br.com.efilho.cora_mobile_test.base.business.data.Order
import br.com.efilho.cora_mobile_test.feature.home.business.interactor.GetOrdersUseCase


class PagedOrdersDataFactory(useCase: GetOrdersUseCase) : DataSource.Factory<Int, Order>() {

    val mutableLiveData = MutableLiveData<PagedOrdersSource>()
    private val pagedOrdersSource = PagedOrdersSource(useCase)

    override fun create(): DataSource<Int, Order> {
        mutableLiveData.postValue(pagedOrdersSource)
        return pagedOrdersSource
    }
}

