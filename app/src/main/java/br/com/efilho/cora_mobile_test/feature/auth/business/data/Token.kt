package br.com.efilho.cora_mobile_test.feature.auth.business.data

data class Token(
    private val accessToken: String,
    val id: String){

    fun getAccessToken() = "Oauth $accessToken"
}
