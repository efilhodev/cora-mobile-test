package br.com.efilho.cora_mobile_test.plugin.main

import android.app.Application
import br.com.efilho.cora_mobile_test.feature.auth.gateway.di.AuthViewModelInjector
import br.com.efilho.cora_mobile_test.feature.home.gateway.di.OrderListViewModelInjector
import br.com.efilho.cora_mobile_test.plugin.dagger.auth.AuthViewModelInjectorImpl
import br.com.efilho.cora_mobile_test.plugin.dagger.home.OrderListViewModelInjectorImpl

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initDependencyInjection()
    }

    private fun initDependencyInjection() {
        AuthViewModelInjector.injector =
            AuthViewModelInjectorImpl(baseContext)

        OrderListViewModelInjector.injector =
            OrderListViewModelInjectorImpl(baseContext)
    }
}