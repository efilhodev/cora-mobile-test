package br.com.efilho.cora_mobile_test.plugin.dagger.auth

import android.content.Context
import br.com.efilho.cora_mobile_test.feature.auth.gateway.di.AuthViewModelInjector
import br.com.efilho.cora_mobile_test.feature.auth.gateway.mvvm.AuthViewModel

class AuthViewModelInjectorImpl(context: Context) : AuthViewModelInjector {

    private val injector: AuthViewModelComponent = DaggerAuthViewModelComponent.builder()
        .authUseCaseModule(AuthUseCaseModule(context))
        .build()

    override fun inject(dependency : AuthViewModel) {
        injector.inject(dependency)
    }
}