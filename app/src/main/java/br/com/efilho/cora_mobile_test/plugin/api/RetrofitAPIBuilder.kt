package br.com.efilho.cora_mobile_test.plugin.api


import com.google.gson.Gson
import okhttp3.Interceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitAPIBuilder(baseUrl: String, interceptors: List<Interceptor> = emptyList()):
AbstractBuilder<API>(baseUrl, interceptors){

    override fun build(): API {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(createHttpClient())
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
            .create(API::class.java)
    }
}