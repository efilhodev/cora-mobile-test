package br.com.efilho.cora_mobile_test.base.business.data

data class CheckoutPrefrences(
    val installments: List<Installment>,
    val redirectUrls: RedirectUrls
)