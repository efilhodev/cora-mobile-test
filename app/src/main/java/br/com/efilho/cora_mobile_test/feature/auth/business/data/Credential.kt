package br.com.efilho.cora_mobile_test.feature.auth.business.data

import com.google.gson.annotations.SerializedName

data class Credential(
    @SerializedName("client_id") val clientId: String,
    @SerializedName("client_secret") val clientSecret: String,
    @SerializedName("grant_type") val grantType: String,
    @SerializedName("username") val username: String,
    @SerializedName("password") val password: String,
    @SerializedName("device_id") val deviceId: String,
    @SerializedName("scope") val scope: String
)