package br.com.efilho.cora_mobile_test.base.business.data

data class Event(
    val createdAt: String,
    val description: String,
    val type: String
)