package br.com.efilho.cora_mobile_test.plugin.repository

import br.com.efilho.cora_mobile_test.feature.auth.business.data.Credential
import br.com.efilho.cora_mobile_test.feature.auth.business.data.Token
import br.com.efilho.cora_mobile_test.feature.auth.business.interactor.AuthRepository
import br.com.efilho.cora_mobile_test.plugin.dao.AuthDAO

class AuthRepositoryImpl(url: String, private val dao: AuthDAO) : BaseRepository(url), AuthRepository {

    override fun authenticate(credential: Credential): Token {
        val request = getService().authenticate(
            credential.clientId,
            credential.clientSecret,
            credential.grantType,
            credential.username,
            credential.password,
            credential.deviceId,
            credential.scope
        )

        val token = doApiRequest(request)
        saveToken(token)
        return token
    }

    private fun saveToken(token: Token) {
        dao.saveToken(token)
    }
}