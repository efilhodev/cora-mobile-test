package br.com.efilho.cora_mobile_test.plugin.dao

import android.content.Context
import android.content.SharedPreferences
import br.com.efilho.cora_mobile_test.base.business.exception.AuthenticationException
import br.com.efilho.cora_mobile_test.feature.auth.business.data.Token
import com.google.gson.Gson

class SharedPreferencesDAO(context: Context): AuthDAO {
    private val sharedPreferences: SharedPreferences
    private val name = "cora_application"
    private val tokenPrefName = "token"

    init {
        sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    override fun getToken(): Token {
        val tokenJson = sharedPreferences.getString(tokenPrefName, "")
        if(tokenJson == null || tokenJson.isEmpty()) throw AuthenticationException()
        return Gson().fromJson(tokenJson, Token::class.java)
    }

    override fun saveToken(token: Token) {
        val tokenJson = Gson().toJson(token)
        sharedPreferences.edit().putString(tokenPrefName, tokenJson).apply()
    }

}