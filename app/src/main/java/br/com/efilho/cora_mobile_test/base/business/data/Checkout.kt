package br.com.efilho.cora_mobile_test.base.business.data

data class Checkout(
    val payBoleto: PayRedirect,
    val payCheckout: PayRedirect,
    val payCreditCard: PayRedirect,
    val payOnlineBankDebitItau: PayRedirect
)