package br.com.efilho.cora_mobile_test.base.business.data

data class Receiver(
    val amount: Amount,
    val feePayor: Boolean,
    val moipAccount: MoipAccount,
    val type: String
)