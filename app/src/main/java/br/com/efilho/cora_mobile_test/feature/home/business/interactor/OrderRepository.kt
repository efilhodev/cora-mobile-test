package br.com.efilho.cora_mobile_test.feature.home.business.interactor

import br.com.efilho.cora_mobile_test.base.business.data.Order
import br.com.efilho.cora_mobile_test.feature.home.business.data.PagedOrdersRequest

interface OrderRepository {
    fun getOrders(pagedOrdersRequest: PagedOrdersRequest): List<Order>
    fun getOrderDetail(id: String?): Order
}