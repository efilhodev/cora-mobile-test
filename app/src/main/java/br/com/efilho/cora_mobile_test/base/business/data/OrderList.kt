package br.com.efilho.cora_mobile_test.base.business.data

import com.google.gson.annotations.SerializedName

data class OrderList(
    @SerializedName("_links") val links: Links,
    val orders: List<Order>,
    val summary: Summary
)