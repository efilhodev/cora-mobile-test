package br.com.efilho.cora_mobile_test.plugin.dagger.home

import android.content.Context
import br.com.efilho.cora_mobile_test.feature.home.business.interactor.GetOrdersUseCase
import br.com.efilho.cora_mobile_test.feature.home.business.interactor.OrderRepository
import br.com.efilho.cora_mobile_test.plugin.dao.AuthDAO
import br.com.efilho.cora_mobile_test.plugin.dao.SharedPreferencesDAO
import br.com.efilho.cora_mobile_test.plugin.repository.OrderRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class GetOrdersUseCaseModule(private val context: Context) {

    @Provides
    @Reusable
    fun providerGetOrdersUseCase(repository: OrderRepository): GetOrdersUseCase {
        return GetOrdersUseCase(repository)
    }

    @Provides
    @Reusable
    fun providerOrderRepository( url: String, dao: AuthDAO): OrderRepository {
        return OrderRepositoryImpl(url, dao)
    }

    @Provides
    @Reusable
    fun provideAuthDAO(): AuthDAO {
        return SharedPreferencesDAO(context)
    }

    @Provides
    @Reusable
    fun providerRepositoryUrl(): String {
        return "https://sandbox.moip.com.br/v2/"
    }
}