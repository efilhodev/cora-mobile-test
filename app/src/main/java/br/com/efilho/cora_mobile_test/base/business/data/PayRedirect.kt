package br.com.efilho.cora_mobile_test.base.business.data

data class PayRedirect(
    val redirectHref: String
)