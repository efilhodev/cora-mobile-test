package br.com.efilho.cora_mobile_test.feature.auth.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.fragment_auth.*

import br.com.efilho.cora_mobile_test.R
import br.com.efilho.cora_mobile_test.base.view.BaseFragment
import br.com.efilho.cora_mobile_test.feature.auth.gateway.mvvm.AuthViewModel
import br.com.efilho.cora_mobile_test.feature.home.gateway.mvvm.OrderListViewModel

class AuthFragment : BaseFragment<AuthViewModel>() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_auth, container, false)
    }

    override fun getViewModelClass(): Class<AuthViewModel> {
        return AuthViewModel::class.java
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        click.setOnClickListener { viewModel.authenticate("moip-test-developer@moip.com.br", "testemoip123") }
    }

    override fun handleError(error: Throwable?) {
        super.handleError(error)
    }

    override fun handleSuccess(value: Any?) {
        super.handleSuccess(value)
        NavHostFragment.findNavController(this).navigate(R.id.action_auth_to_home)

    }
}
