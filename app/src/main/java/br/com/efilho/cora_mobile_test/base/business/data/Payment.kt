package br.com.efilho.cora_mobile_test.base.business.data

data class Payment(
    val fundingInstrument: FundingInstrument,
    val id: String,
    val installmentCount: Int
)