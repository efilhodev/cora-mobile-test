package br.com.efilho.cora_mobile_test.base.business.data

data class Phone(
    val areaCode: String,
    val countryCode: String,
    val number: String
)