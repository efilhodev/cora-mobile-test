package br.com.efilho.cora_mobile_test.base.business.data

data class RedirectUrls(
    val urlFailure: String,
    val urlSuccess: String
)