package br.com.efilho.cora_mobile_test.base.business.interactor

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

abstract class AbstractUseCase<in P, R> : Interactor<P, R> {
    lateinit var callback: (R) -> Unit

    fun start(param: P? = null, callback: (R) -> Unit) {
        try {
            this.callback = callback
            process(param)
        } catch (error: Throwable) {
            onError(error)
        }
    }

    protected open fun process(param: P?) {
        if (guard(param)) {
            execute(param).also { onSuccess(it!!) }
        } else {
            onGuardError()
        }
    }

    internal open fun onSuccess(output: R) {
        if (::callback.isInitialized) callback(output)
    }

    internal open fun onGuardError() {}

    internal abstract fun onError(error: Throwable)
}


abstract class UseCase<in P, R> : AbstractUseCase<P, Output<R>>() {

    override fun onError(error: Throwable) {
        callback(ErrorOutput(error))
    }

    fun invoke(param: P? = null, dispatcher: CoroutineDispatcher? = null, callback: (Output<R>) -> Unit): Job? {
        val invoker = UseCaseDispatcher(this)
        return if (dispatcher != null) {
            invoker.dispatch(param, dispatcher, callback)
        } else {
            invoker.dispatch(param, callback = callback)
        }
    }
}