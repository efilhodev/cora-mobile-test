package br.com.efilho.cora_mobile_test.base.business.interactor

interface Interactor<in P, R> {
    fun guard(param: P? = null): Boolean = true
    fun execute(param: P? = null): R
}