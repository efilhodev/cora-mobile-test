package br.com.efilho.cora_mobile_test.plugin.dagger.auth

import br.com.efilho.cora_mobile_test.feature.auth.gateway.mvvm.AuthViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AuthUseCaseModule::class])
interface AuthViewModelComponent {
    fun inject(authViewModel: AuthViewModel)
}