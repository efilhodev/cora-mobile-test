package br.com.efilho.cora_mobile_test.plugin.repository

import br.com.efilho.cora_mobile_test.base.business.data.Order
import br.com.efilho.cora_mobile_test.feature.auth.business.data.Token
import br.com.efilho.cora_mobile_test.feature.home.business.data.PagedOrdersRequest
import br.com.efilho.cora_mobile_test.feature.home.business.interactor.OrderRepository
import br.com.efilho.cora_mobile_test.plugin.dao.AuthDAO

class OrderRepositoryImpl(url: String, private val dao: AuthDAO) : BaseRepository(url), OrderRepository {

    override fun getOrders(pagedOrdersRequest: PagedOrdersRequest): List<Order> {
        val token = getToken()
        val call = getService().getOrders(
            token.getAccessToken(),
            pagedOrdersRequest.filters,
            pagedOrdersRequest.limit,
            pagedOrdersRequest.offset
        )
        return doApiRequest(call).orders
    }

    private fun getToken(): Token {
        return dao.getToken()
    }

    override fun getOrderDetail(id: String?): Order {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}