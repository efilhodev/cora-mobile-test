package br.com.efilho.cora_mobile_test.plugin.api


import br.com.efilho.cora_mobile_test.base.business.data.OrderList
import br.com.efilho.cora_mobile_test.feature.auth.business.data.Credential
import br.com.efilho.cora_mobile_test.feature.auth.business.data.Token
import retrofit2.Call
import retrofit2.http.*

interface API {

    @FormUrlEncoded
    @POST("oauth/token")
    fun authenticate(
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("grant_type") grantType: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("device_id") deviceId: String,
        @Field("scope") scope: String
    ): Call<Token>


    @GET("orders")
    fun getOrders(
        @Header("Authorization") token: String,
        @Query("filters") filters: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Call<OrderList>


    @GET("orders/{orderId}")
    fun getOrderDetail(
        @Header("Authorization") token: String,
        @Path("orderId") orderId: String): Call<Any>
}