package br.com.efilho.cora_mobile_test.plugin.dao

import br.com.efilho.cora_mobile_test.feature.auth.business.data.Token

interface AuthDAO {
    fun getToken(): Token
    fun saveToken(token: Token)
}