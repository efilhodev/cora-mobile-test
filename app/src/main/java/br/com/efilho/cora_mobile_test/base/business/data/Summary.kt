package br.com.efilho.cora_mobile_test.base.business.data

data class Summary(
    val amount: Int,
    val count: Int
)