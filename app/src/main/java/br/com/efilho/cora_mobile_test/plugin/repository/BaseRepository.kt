package br.com.efilho.cora_mobile_test.plugin.repository

import br.com.efilho.cora_mobile_test.base.business.exception.AuthenticationException
import br.com.efilho.cora_mobile_test.base.business.exception.HttpException
import br.com.efilho.cora_mobile_test.base.business.exception.InternetConnectionException
import br.com.efilho.cora_mobile_test.plugin.api.API
import br.com.efilho.cora_mobile_test.plugin.api.RetrofitAPIBuilder
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import java.io.IOException


abstract class BaseRepository(private val baseUrl: String) {


    protected fun <R> doApiRequest(request: Call<R>): R {
        try {
            val response = request.execute()
            if (response.isSuccessful) return response.body()!!
            if (response.code() == 401) {
                throw AuthenticationException()
            }
            throw HttpException(response.code(), response.message())
        } catch (e: IOException) {
            e.printStackTrace()
            throw InternetConnectionException()
        }
    }

    internal open fun getService(interceptors: List<Interceptor> = emptyList()): API {
        val updatedInterceptors = mutableListOf<Interceptor>(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BASIC
        }
        ).apply { addAll(interceptors) }

        return RetrofitAPIBuilder(baseUrl, updatedInterceptors).build()
    }
}
