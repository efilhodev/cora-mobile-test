package br.com.efilho.cora_mobile_test.base.gateway.mvvm

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import br.com.efilho.cora_mobile_test.base.business.interactor.CompositeJobDisposable
import br.com.efilho.cora_mobile_test.base.business.interactor.Output
import br.com.efilho.cora_mobile_test.base.business.interactor.UseCase
import kotlinx.coroutines.Job

open class ViewState(val output: Output<*>, var handled: Boolean = false) {
    fun isError(): Boolean = output.isError()
    fun isEmpty(): Boolean = output.isEmpty()
    fun isSuccess(): Boolean = output.isSuccess()
}

abstract class BaseViewModel : ViewModel() {
    protected val channels: MutableMap<String, MutableLiveData<ViewState>> = mutableMapOf()
    protected val availableChannels = mutableListOf<String>()
    protected val compositeJobDisposable = CompositeJobDisposable()

    init {
        onCreate()
    }

    private fun onCreate() {
        declareChannels()
    }

    protected abstract fun declareChannels()

    fun observe(channelName: String, owner: LifecycleOwner, listener: Observer<in ViewState>) {
        createChannelIfNeeded(channelName)
        channels[channelName]?.observe(owner, listener)
    }

    fun getChannels(): List<String> {
        return availableChannels
    }

    fun disposeAll() {
        compositeJobDisposable.cancel()
    }

    protected fun <P, R> invoke(
        useCase: UseCase<P, R>,
        param: P? = null,
        listener: ((Output<R>) -> Unit)? = null
    ): Job? {
        val job = useCase.invoke(param) {
            listener?.run {
                this(it)
            }
        }
        compositeJobDisposable.add(job)
        return job
    }

    fun postValue(channelName: String, output: Output<*>) {
        val viewState = ViewState(output)
        val channel = channels[channelName]
        channel?.postValue(viewState)
    }

    private fun createChannelIfNeeded(channelName: String) {
        if (availableChannels.contains(channelName) && channels[channelName] == null) {
            channels[channelName] = MutableLiveData()
        }
    }
}