package br.com.efilho.cora_mobile_test.plugin.dagger.home

import android.content.Context
import br.com.efilho.cora_mobile_test.feature.home.gateway.di.OrderListViewModelInjector
import br.com.efilho.cora_mobile_test.feature.home.gateway.mvvm.OrderListViewModel

class OrderListViewModelInjectorImpl(context: Context) : OrderListViewModelInjector {

    private val injector: OrderListViewModelComponent = DaggerOrderListViewModelComponent.builder()
        .getOrdersUseCaseModule(GetOrdersUseCaseModule(context))
        .build()

    override fun inject(dependency: OrderListViewModel) {
        injector.inject(dependency)
    }
}