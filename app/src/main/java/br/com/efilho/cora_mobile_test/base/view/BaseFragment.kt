package br.com.efilho.cora_mobile_test.base.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.efilho.cora_mobile_test.base.business.exception.AuthenticationException
import br.com.efilho.cora_mobile_test.base.business.exception.HttpException
import br.com.efilho.cora_mobile_test.base.business.exception.InternetConnectionException
import br.com.efilho.cora_mobile_test.base.gateway.mvvm.BaseViewModel
import br.com.efilho.cora_mobile_test.base.gateway.mvvm.ViewState

abstract class BaseFragment<V: BaseViewModel>: Fragment() {
    protected lateinit var viewModel: V
    protected open val activityScoped = false

    protected abstract fun getViewModelClass(): Class<V>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        observeViewModel()
    }

    protected open fun setupViewModel() {
        val clazz = getViewModelClass()
        viewModel = if(activityScoped) {
            ViewModelProviders.of(requireActivity()).get(clazz)
        } else {
            ViewModelProviders.of(this).get(clazz)
        }
    }

    protected open fun observeViewModel() {
        observeAllChannels()
    }

    protected fun observeAllChannels() {
        viewModel.getChannels().forEach { observeChannel(it) }
    }

    protected fun observeChannel(channelName: String) {
        viewModel.observe(channelName,this, Observer { state-> handleResponse(state) })
    }

    protected open fun handleResponse(state: ViewState) {
        if(!state.handled)  {
            hideLoading()
            state.handled = true
            if (state.isError()) {
                handleThrowable(state.output.error)
            } else {
                handleSuccess(state.output.value)
            }
        }
    }

    protected open fun showLoading() {}

    protected open fun hideLoading() {}

    private fun handleThrowable(error: Throwable?) {
        when(error) {
            is AuthenticationException -> handleAuthError()
            is HttpException -> handleHttpError(error)
            is InternetConnectionException -> handleConnectionError()
            else -> handleError(error)
        }
    }

    protected open fun handleAuthError() {}

    protected open fun handleHttpError(error: HttpException) {}

    protected open fun handleConnectionError() {}

    protected open fun handleError(error: Throwable?) {}

    protected open fun handleSuccess(value: Any?) {}

    protected fun setupToolbar(toolbar: Toolbar, homeAsUpEnabled: Boolean = false) {
        (activity as? BaseActivity)?.resetToolbar(toolbar, homeAsUpEnabled)
    }
}