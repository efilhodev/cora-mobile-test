package br.com.efilho.cora_mobile_test.feature.auth.gateway.di

import br.com.efilho.cora_mobile_test.base.gateway.di.Injector
import br.com.efilho.cora_mobile_test.feature.auth.gateway.mvvm.AuthViewModel

interface AuthViewModelInjector : Injector<AuthViewModel> {
    companion object{
        lateinit var injector: AuthViewModelInjector
    }
}