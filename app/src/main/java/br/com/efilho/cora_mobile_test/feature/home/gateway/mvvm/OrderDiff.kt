package br.com.efilho.cora_mobile_test.feature.home.gateway.mvvm

import androidx.recyclerview.widget.DiffUtil
import br.com.efilho.cora_mobile_test.base.business.data.Order

object OrderDiff {
    val DIFF_CALLBACK: DiffUtil.ItemCallback<Order> = object : DiffUtil.ItemCallback<Order>() {
        override fun areItemsTheSame(oldItem: Order, newItem: Order): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Order, newItem: Order): Boolean {
            return oldItem == newItem
        }
    }
}