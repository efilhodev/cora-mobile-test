package br.com.efilho.cora_mobile_test.feature.auth.gateway.mvvm

import br.com.efilho.cora_mobile_test.base.business.interactor.Output
import br.com.efilho.cora_mobile_test.base.gateway.mvvm.BaseViewModel
import br.com.efilho.cora_mobile_test.feature.auth.business.data.Credential
import br.com.efilho.cora_mobile_test.feature.auth.business.data.Token
import br.com.efilho.cora_mobile_test.feature.auth.business.interactor.AuthUseCase
import br.com.efilho.cora_mobile_test.feature.auth.gateway.di.AuthViewModelInjector
import javax.inject.Inject

class AuthViewModel : BaseViewModel() {

    init {
        AuthViewModelInjector.injector.inject(this)
    }

    @Inject
    lateinit var authUseCase: AuthUseCase

    override fun declareChannels() {
        availableChannels.add(AUTHENTICATION_CHANNEL)
    }

    fun authenticate(email: String, password: String) {
        val credential = Credential(
            "APP-H1DR0RPHV7SP", "05acb6e128bc48b2999582cd9a2b9787",
            "password", email, password, "156465321556487", "APP_ADMIN"
        )

        invoke(authUseCase, credential, this::onAuthResponse)
    }

    private fun onAuthResponse(output: Output<Token>) {
        postValue(AUTHENTICATION_CHANNEL, output)
    }

    companion object {
        private const val AUTHENTICATION_CHANNEL = "AUTHENTICATION_CHANNEL"
    }
}