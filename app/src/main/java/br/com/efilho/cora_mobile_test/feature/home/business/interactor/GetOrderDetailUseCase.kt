package br.com.efilho.cora_mobile_test.feature.home.business.interactor

import br.com.efilho.cora_mobile_test.base.business.interactor.Output
import br.com.efilho.cora_mobile_test.base.business.interactor.UseCase
import br.com.efilho.cora_mobile_test.base.business.interactor.ValueOutput
import br.com.efilho.cora_mobile_test.base.business.data.Order

class GetOrderDetailUseCase (private val repository: OrderRepository): UseCase<String, Order>() {

    override fun execute(param: String?): Output<Order> {
        val order = repository.getOrderDetail(param)
        return ValueOutput(order)
    }
}