package br.com.efilho.cora_mobile_test.feature.home.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.efilho.cora_mobile_test.R
import br.com.efilho.cora_mobile_test.base.business.data.Order
import br.com.efilho.cora_mobile_test.base.view.BaseFragment
import br.com.efilho.cora_mobile_test.feature.home.gateway.mvvm.OrderListViewModel
import kotlinx.android.synthetic.main.fragment_auth.*
import kotlinx.android.synthetic.main.fragment_auth.click
import kotlinx.android.synthetic.main.frament_home.*

class HomeFragment : BaseFragment<OrderListViewModel>() {
    private val adapter = OrderListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frament_home, container, false)
    }

    override fun getViewModelClass(): Class<OrderListViewModel> {
        return OrderListViewModel::class.java
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        click.setOnClickListener { viewModel.getOrders() }

        rv_orders.layoutManager = LinearLayoutManager(context)
        rv_orders.adapter = adapter
    }

    override fun handleError(error: Throwable?) {
        super.handleError(error)
    }

    override fun handleSuccess(value: Any?) {
        if (value is PagedList<*>) {
            adapter.submitList(value as PagedList<Order>?)
        }
    }
}