package br.com.efilho.cora_mobile_test.base.gateway.di

import br.com.efilho.cora_mobile_test.base.gateway.mvvm.BaseViewModel

interface Injector<D : BaseViewModel> {
    fun inject(dependency : D)
}