package br.com.efilho.cora_mobile_test.base.business.data

data class Amount(
    val currency: String,
    val fees: Int,
    val liquid: Int,
    val otherReceivers: Int,
    val paid: Int,
    val refunds: Int,
    val subtotals: Subtotals,
    val total: Int
)