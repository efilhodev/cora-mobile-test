package br.com.efilho.cora_mobile_test.base.view

import android.os.Bundle
import br.com.efilho.cora_mobile_test.R
import br.com.efilho.cora_mobile_test.base.view.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
    }
}
