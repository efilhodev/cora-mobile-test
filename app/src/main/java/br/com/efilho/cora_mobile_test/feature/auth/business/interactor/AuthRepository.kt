package br.com.efilho.cora_mobile_test.feature.auth.business.interactor

import br.com.efilho.cora_mobile_test.feature.auth.business.data.Credential
import br.com.efilho.cora_mobile_test.feature.auth.business.data.Token

interface AuthRepository{
    fun authenticate(credential: Credential): Token
}