package br.com.efilho.cora_mobile_test.base.business.data

data class Installment(
    val addition: Int,
    val quantity: List<Int>
)