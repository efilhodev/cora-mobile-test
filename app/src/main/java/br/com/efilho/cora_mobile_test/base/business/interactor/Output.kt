package br.com.efilho.cora_mobile_test.base.business.interactor

abstract class Output<V>(val value: V? = null, val error: Throwable? = null) {
    open fun isError(): Boolean = error != null
    open fun isSuccess(): Boolean = !isError()
    fun isEmpty(): Boolean = value != null
}

open class ValueOutput<V>(value: V? = null) : Output<V>(value, null) {
    override fun isError(): Boolean = false
}

class ErrorOutput<V>(error: Throwable?, value: V? = null) : Output<V>(value, error) {
    override fun isError(): Boolean = true
}