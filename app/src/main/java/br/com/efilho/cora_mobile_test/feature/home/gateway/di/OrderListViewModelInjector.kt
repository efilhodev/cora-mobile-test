package br.com.efilho.cora_mobile_test.feature.home.gateway.di

import br.com.efilho.cora_mobile_test.base.gateway.di.Injector
import br.com.efilho.cora_mobile_test.feature.home.gateway.mvvm.OrderListViewModel

interface OrderListViewModelInjector : Injector<OrderListViewModel>{
    companion object{
        lateinit var injector: OrderListViewModelInjector
    }
}